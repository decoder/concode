import sys


def score(gold, data):
    exact = 0
    total = 0
    with open(gold, 'r') as f:
        with open(data, 'r') as d:
            for line in d:
                g = f.readline()
                if g.strip() == line.strip():
                    exact += 1
                total += 1

    return exact * 100.0/total if total > 0 else 0
