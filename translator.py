import argparse
import json
import os
import sys
import tempfile
import torch
from typing import List

from preprocess import Vocab, CDDataset
from S2SModel import S2SModel


class Translator:
    def __init__(self, model):
        parser = argparse.ArgumentParser(description='translator.py')

        parser.add_argument('-model', required=True,
                            help='Path to model .pt file')
        parser.add_argument('-test', required=False, help='')
        parser.add_argument('-beam_size',  type=int, default=5,
                            help='Beam size')
        parser.add_argument('-batch_size', type=int, default=1,
                            help='Batch size')
        parser.add_argument('-max_sent_length', type=int, default=100,
                            help='Maximum sentence length.')
        parser.add_argument('-replace_unk', action="store_true",
                            help="""Replace the generated UNK tokens with the
                            source token that had highest attention weight. If
                            phrase_table is provided, it will lookup the
                            identified source token and give the corresponding
                            target token.
                            If it is not provided (or the identified source
                            token does not exist in the table) then it will
                            copy the source token""")

        opt = parser.parse_args()
        opt.model = model
        opt.beam_size = 3
        opt.batch_size = 1
        opt.max_sent_length = 500
        opt.replace_unk = True

        self.opt = opt
        self.items_list = []

        self.device = torch.device(
          'cuda' if os.environ.get('CUDA_VISIBLE_DEVICES') is not None
          else 'cpu')
        # print(f"With cuda: {torch.cuda.is_available()}", flush=True)
        if torch.cuda.is_available():
            map_location = lambda storage, loc: storage.to(self.device)
        else:
            map_location = 'cpu'
        print(f"Loading model: {opt.model}", flush=True)
        self.checkpoint = torch.load(opt.model, map_location=map_location)
        vocabs = self.checkpoint['vocab']
        vocabs['mask'] = vocabs['mask'].to(self.device)

        self.model = S2SModel(self.checkpoint['opt'], vocabs,
                              self.device).to(self.device)
        self.model.load_state_dict(self.checkpoint['model'])
        self.model.eval()

    def translate(self, nl: List[str],
                  var_names: List[str], var_types: List[str],
                  method_names: List[str], method_returns: List[str]):
        """
        nl: the tokenized text to translate
        var_names: the list of variable names. Must be not empty.
        var_types: the list of variable types. Must have the same length as
                   var_names
        method_names: the list of method names. Must be not empty.
        method_returns: the list of methods return types. Must have the same
                        length as method_names
        """

        if len(var_names) == 0 or len(method_names) == 0:
            print(f"Cannot translate with no context", file=sys.stderr)
            raise ValueError(f"Cannot translate with no context")
        if (len(var_names) != len(var_types)
                or len(method_names) != len(method_returns)):
            error_msg = (f"Incoherent parameter lists sizes: "
                         f"var_names={len(var_names)}, "
                         f"var_types={len(var_types)}, "
                         f"method_names={len(method_names)}, "
                         f"method_returns={len(method_returns)}")
            print(error_msg, file=sys.stderr)
            raise ValueError(error_msg)

        js = {
            "nl": nl,
            "varNames": var_names,
            "varTypes": var_types,
            "methodNames": method_names,
            "methodReturns": method_returns,
            "code": [],
            "idx": 0,
            "rules": [],
            "methodParamNames": [],
            "methodParamTypes": [],
            "seq2seq": [],
            "seq2seq_nop": [],
          }

        # print(f"Creating test dataset: {js}", flush=True)
        fo = tempfile.NamedTemporaryFile("w")
        json.dump([js], fo)
        fo.flush()

        test = CDDataset(fo.name, None, test=True, trunc=False)
        fo.close()
        test.toNumbers(self.checkpoint['vocab'])

        # print(f"Computing test batches", flush=True)
        total_test = test.compute_batches(self.opt.batch_size,
                                          self.checkpoint['vocab'],
                                          self.checkpoint['opt'].max_camel,
                                          0, 1,
                                          self.checkpoint['opt'].decoder_type,
                                          randomize=False, no_filter=True)
        print(f'Options: {self.model.opt}, {self.opt}', flush=True)

        predictions = []
        for idx, batch in enumerate(test.batches):  # For each batch
            predictions.append(self.model.predict(batch,
                                                  self.opt, None).prediction)
        return predictions


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='translator.py')

    parser.add_argument('-model', required=True,
                        help='Path to model .pt file')
    parser.add_argument('-test', required=True,
                        help='Path to file to translate')

    opt = parser.parse_args()
    tranlator = Translator(opt.model)
    with open(opt.test) as file:
        js = json.load(file)
        print(js)
        prediction = tranlator.translate(
          nl=js["nl"],
          var_names=js["varNames"],
          var_types=js["varTypes"],
          method_names=js["methodNames"],
          method_returns=js["methodReturns"])

    print(f"Prediction: {prediction}", flush=True)
