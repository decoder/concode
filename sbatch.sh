#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

LOG=$(sbatch $1 | awk '{print $4}')
i=1
declare -a progress=("/" "-" "\\" "-")
while [ ! -f res_${LOG}.log ]; do
    /bin/echo -n -e "Waiting for res_${LOG}.log to appear ${progress[$((${i} % 4))]} \r"
    i=$((${i}+1))
    sleep 1
done
echo
tail -f res_${LOG}.log

